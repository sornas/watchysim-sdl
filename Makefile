CXX = clang++
CPPFLAGS = -DWATCHY_SIM -std=c++17 -g -Iwatchy -Ilib -Ilib/imgui -Icommon $(shell pkg-config --cflags sdl2)
LDFLAGS = -g $(shell pkg-config --libs sdl2)

SRC = watchy/Watchy.cpp \
      watchy/WatchySim.cpp \
      watchy/WatchySim-sdl.cpp \
      lib/arduino/Common.cpp \
      lib/arduino/String.cpp \
      lib/arduino/dtostrf.cpp \
      $(wildcard lib/imgui/imgui*.cpp) \
      lib/imgui/backends/imgui_impl_sdl2.cpp \
      lib/imgui/backends/imgui_impl_sdlrenderer2.cpp \
      $(shell find faces -name "*.cpp")
OBJ = ${SRC:.cpp=.o}

.cpp.o:
	${CXX} -c ${CPPFLAGS} $< -o $@

watchysim: ${OBJ}
	${CXX} -o $@ ${OBJ} ${LDFLAGS}

.PHONY: clean
clean:
	rm -rf ${OBJ} watchysim
