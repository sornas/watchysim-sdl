# WatchySim-SDL

This is a simulator for the ESP32-based open source e-paper watch
[Watchy](https://watchy.sqfmi.com/). It was forked from
[WatchySim](https://github.com/LeeHolmes/watchysim) but uses SDL instead of
Windows API.

For more information about WatchySim, for now please refer to [the WatchySim
README](https://github.com/LeeHolmes/watchysim/blob/main/README.md).

## Building WatchySim-SDL on Linux

Make sure you have SDL2 installed and run `make`.

## Current status

Works:

- Initializing faces
- Tested in-repo faces:
  - 7_SEG
  - AnalogGabel

Planned:

- Stimulate inputs (clock, buttons, network)

Ideas for the future:

- Debug draw, shows which pixels are redrawn
- Port-forward WiFi-connections
- Dynamically switch between different faces
- Headless mode, for testing faces in CI

## License

The original code is licensed under the permissive MIT license. Its copyright
notice can be found in the file `LICENSE-WatchySim`. This repository is licensed
under the GPL, and its copyright notice can be foun in the file
`LICENSE-WatchySim-SDL`.
