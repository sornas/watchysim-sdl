#pragma once

#include <stdint.h>

typedef uint8_t ScreenBuffer[200*200];

void set_pixel(ScreenBuffer *buf, int x, int y, uint8_t c);

// struct Screen {
//     uint8_t buffer[200*200*3];
// };
