#include <time.h>

#include "imgui/imgui.h"
#include "imgui/backends/imgui_impl_sdl2.h"
#include "imgui/backends/imgui_impl_sdlrenderer2.h"
#include <SDL2/SDL.h>

#include "Watchy.h"
#include "WatchySim.h"

// Faces
#include "../faces/7_SEG/Watchy_7_SEG.h"
#include "../faces/AnalogGabel/Watchy_AnalogGabel.h"
#include "../faces/DOS/Watchy_DOS.h"
#include "../faces/DrawTest/Watchy_Draw_Test.h"
#include "../faces/MacPaint/Watchy_MacPaint.h"
#include "../faces/Mario/Watchy_Mario.h"
#include "../faces/Niobe/niobe.h"
#include "../faces/Pokemon/Watchy_Pokemon.h"
#include "../faces/PowerShell/Watchy_PowerShell.h"
#include "../faces/Scene/Watchy_scene.h"
#include "../faces/Tetris/Watchy_Tetris.h"

namespace WatchySim {

// Watchy watchy = Watchy(); // Basic
// Watchy7SEG watchy = Watchy7SEG();
// WatchyAnalogGabel watchy = WatchyAnalogGabel();
// WatchyDOS watchy = WatchyDOS();
// WatchyDrawTest watchy = WatchyDrawTest();
// WatchyMacPaint watchy = WatchyMacPaint();
// WatchyMario watchy = WatchyMario();
// Niobe watchy = Niobe();
// WatchyPokemon watchy = WatchyPokemon();
// WatchyPowerShell watchy = WatchyPowerShell();
// Scene watchy = Scene();
WatchyTetris watchy = WatchyTetris();

ScreenBuffer screen_buffer = {0};

SDL_Renderer *renderer;
SDL_Window *window;

void init() {
    // Initialize SDL
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    window = SDL_CreateWindow(
        "WatchySim",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        600, 600,
        window_flags
    );
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);

    // Initialize ImGui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup ImGui style
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    ImGui::StyleColorsDark();

    // Setup ImGui for SDL2
    ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
    ImGui_ImplSDLRenderer2_Init(renderer);

    // Intialize Watchy
    time_t curr_time = time(NULL);
    struct tm *tm_local = localtime(&curr_time);
    watchy.setTime(*tm_local);
}

void update() {
    static bool show_imgui_demo_window = false;

    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("Help")) {
            if (ImGui::MenuItem("Show demo window", "", show_imgui_demo_window))
                show_imgui_demo_window = !show_imgui_demo_window;
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }

    if (show_imgui_demo_window) {
        ImGui::ShowDemoWindow(&show_imgui_demo_window);
    }
}

void draw() {
    ImGui::Render();

    // Clear entire screen
    SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
    SDL_RenderClear(renderer);

    // Update screen buffer using current watch face
    watchy.showWatchFace(&screen_buffer);

    // Render screen buffer, one pixel at a time
    for (int y = 0; y < 200; y++) {
        for (int x = 0; x < 200; x++) {
            SDL_Rect rect;
            rect.x = x * 3;
            rect.y = y * 3;
            rect.w = 3;
            rect.h = 3;
            if (screen_buffer[y * 200 + x])
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            else
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderFillRect(renderer, &rect);
        }
    }

    ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());

    SDL_RenderPresent(renderer);
}

} // namespace WatchySim

int main() {
    using namespace WatchySim;

    init();
    bool running = true;
    SDL_Event event;

    while (running) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) running = false;

            ImGui_ImplSDL2_ProcessEvent(&event);
        }
        // Start the ImGui frame
        ImGui_ImplSDLRenderer2_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();

        update();
        draw();
        SDL_Delay(1);
    }
}
